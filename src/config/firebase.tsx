import firebase from "firebase";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";

// const {
//   REACT_APP_FIREBASE_KEY,
//   REACT_APP_FIREBASE_DOMAIN,
//   REACT_APP_FIREBASE_DATABASE,
//   REACT_APP_FIREBASE_PROJECT_ID,
//   REACT_APP_FIREBASE_STORAGE_BUCKET,
//   REACT_APP_FIREBASE_SENDER_ID,
//   REACT_APP_FIREBASE_APP_ID,
//   REACT_APP_FIREBASE_MEASUREMENT_ID,
// } = process.env;

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBsubyrVkkL2Irsg5XBjGc94FNmVe4lgbo",
  authDomain: "shopify-366cb.firebaseapp.com",
  databaseURL: "https://shopify-366cb.firebaseio.com",
  projectId: "shopify-366cb",
  storageBucket: "shopify-366cb.appspot.com",
  messagingSenderId: "840709091203",
  appId: "1:840709091203:web:3a95a04a51a0f313b650b6",
  measurementId: "G-Z27X05LZ02"
};

// Initialize Firebase
const app = firebase.initializeApp(firebaseConfig);

const provider = {
  facebook: new firebase.auth.FacebookAuthProvider(),
  google: new firebase.auth.GoogleAuthProvider(),
  github: new firebase.auth.GithubAuthProvider(),
};

export { app, provider };
