# Shopify

A custom shopify that is built in firebase and netlify lambda along with tailwindCss

![demo](https://github.com/zneret03/Shopify/blob/master/src/static/frontpage.png)

Built with :

- [reactjs](https://reactjs.org/)
- [tailwindcss](https://tailwindcss.com/)
- [chartjs](https://www.chartjs.org/)
- [react-spring](https://www.react-spring.io/)
- [Antd](https://ant.design/)
- [Firebase](https://firebase.google.com/)
- [netlify-lambda](https://www.netlify.com/products/functions/)
- [react-flip-move](https://github.com/joshwcomeau/react-flip-move)

### Getting Started

1. Install dependencies

   `npm install`

2. Development

   `npm run start`
